<?php
require "DataBaseConfig.php";

class DataBase
{
    public $connect;
    public $data;
    private $sql;
    protected $servername;
    protected $username;
    protected $password;
    protected $databasename;

    public function __construct()
    {
        $this->connect = null;
        $this->data = null;
        $this->sql = null;
        $dbc = new DataBaseConfig();
        $this->servername = $dbc->servername;
        $this->username = $dbc->username;
        $this->password = $dbc->password;
        $this->databasename = $dbc->databasename;
    }

    function dbConnect()
    {
        $this->connect = mysqli_connect($this->servername, $this->username, $this->password, $this->databasename);
        return $this->connect;
        mysqli_query($connect,"SET NAMES 'utf8'");      
    }

    function prepareData($data)
    {
        return mysqli_real_escape_string($this->connect, stripslashes(htmlspecialchars($data)));
    }

    function logIn($users, $tk, $mk)
    {
        $tk = $this->prepareData($tk);
        $mk = $this->prepareData($mk);
        $this->sql = "select * from " . $users . " where tk = '" . $tk . "'";
        $result = mysqli_query($this->connect, $this->sql);
        $row = mysqli_fetch_assoc($result);
        if (mysqli_num_rows($result) != 0) {
            $dbusername = $row['tk'];
            $dbpassword = $row['mk'];
            if ($dbusername == $tk && password_verify($mk, $dbpassword)) {
                $login = true;
            } else $login = false;
        } else $login = false;

        return $login;
    }

    function signUp($users, $hoten, $email, $tk, $mk)
    {
        $hoten = $this->prepareData($hoten);
        $tk = $this->prepareData($tk);
        $mk = $this->prepareData($mk);
        $email = $this->prepareData($email);
        $mk = password_hash($mk, PASSWORD_DEFAULT);
        $this->sql =
            "INSERT INTO " . $users . " (hoten, tk, mk, email) VALUES ('" . $hoten . "','" . $tk . "','" . $mk . "','" . $email . "')";
        if (mysqli_query($this->connect, $this->sql)) {
            return true;
        } else return false;
    }

}

?>
