-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th6 26, 2021 lúc 10:07 AM
-- Phiên bản máy phục vụ: 10.4.19-MariaDB
-- Phiên bản PHP: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `shop`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `loaimonan`
--

CREATE TABLE `loaimonan` (
  `id` int(11) NOT NULL,
  `tenloai` varchar(200) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `hinhanh` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `loaimonan`
--

INSERT INTO `loaimonan` (`id`, `tenloai`, `hinhanh`) VALUES
(1, 'Bún', 'https://i.ibb.co/wh1qfqQ/image.png'),
(2, 'Cơm', 'https://i.ibb.co/smd2N7J/image.png');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sanpham`
--

CREATE TABLE `sanpham` (
  `id` int(3) NOT NULL,
  `tensp` varchar(1000) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `gia` int(15) NOT NULL,
  `motasp` varchar(1000) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `hinhanh` varchar(200) NOT NULL,
  `idsp` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `sanpham`
--

INSERT INTO `sanpham` (`id`, `tensp`, `gia`, `motasp`, `hinhanh`, `idsp`) VALUES
(1, 'Đầm Dây kéo Họa tiết hoa Boho', 314000, 'Phong cách:	Boho\r\nMàu sắc:	Màu be\r\nKiểu mẫu:	Họa tiết hoa\r\nChiều dài:	Ngắn\r\nKiểu:	Phù hợp\r\nChi tiết:	Xù, Viên lá sen, Dây kéo\r\nLoại Phù hợp:	Phù hợp thường\r\nViền :	Vòng cổ\r\nChiều dài tay:	Ngắn tay\r\nLoại tay áo:	Ống tay áo\r\nVòng eo:	Vòng eo cao\r\nMùa:	Mùa Hè\r\nMỏng:	Không\r\nHình hem:	Lá sen\r\nVật liệu:	Polyester\r\nThành phần:	100% Polyester\r\nSợi vải:	Không căng', 'https://i.ibb.co/D47snrj/v1.png', 1),
(2, 'Áo sơ mi Kẻ sọc', 164000, 'Phong cách:	Dễ thương\r\nMàu sắc:	Màu xanh hải quân\r\nKiểu mẫu:	Kẻ sọc\r\nChiều dài:	Thường xuyên\r\nKiểu:	Ngọn\r\nLoại Phù hợp:	Phù hợp thường\r\nViền :	Cổ vuông\r\nChiều dài tay:	Nửa tay\r\nLoại tay áo:	Ống tay áo\r\nMùa:	Mùa Hè\r\nMỏng:	Không\r\nLoại túi váy:	Áo chui\r\nVật liệu:	Polyester\r\nThành phần:	75% Polyester, 25% Bông\r\nSợi vải:	Không căng', 'https://i.ibb.co/T8f6nQW/v2.png', 1),
(3, 'Áo sơ mi Nam Nút phía trước Báo Đường phố', 396000, 'Phong cách:	Đường phố\r\nMàu sắc:	Nhiều màu\r\nKiểu mẫu:	Báo\r\nChiều dài:	Thường xuyên\r\nKiểu:	Áo sơ mi\r\nChi tiết:	Nút phía trước\r\nLoại Phù hợp:	Quá lớn\r\nViền :	Vòng cổ\r\nChiều dài tay:	Nửa tay\r\nLoại tay áo:	tay cánh\r\nMùa:	Mùa Xuân/Mùa Hè\r\nMỏng:	Không\r\nLoại túi váy:	Không nút\r\nVật liệu:	Polyester\r\nThành phần:	89.6% Polyester, 10.4% Bông vải thun\r\nSợi vải:	Căng nhẹ', 'https://i.ibb.co/FstnBF8/v3.png', 2),
(4, 'Áo sơ mi Nam Nút phía trước màu trơn Đường phố', 440000, 'Phong cách:	Đường phố\r\nMàu sắc:	Màu Khaki\r\nKiểu mẫu:	màu trơn\r\nChiều dài:	Thường xuyên\r\nKiểu:	Áo sơ mi\r\nChi tiết:	Nút phía trước\r\nLoại Phù hợp:	Phù hợp thường\r\nViền :	Cổ Polo\r\nChiều dài tay:	Ngắn tay\r\nLoại tay áo:	Tay thường\r\nMùa:	Mùa Hè\r\nMỏng:	Không\r\nLoại túi váy:	Cửa nửa mở\r\nVật liệu:	Nylon\r\nThành phần:	86.7% Polyamide, 13.3% Bông vải thun\r\nSợi vải:	Căng nhẹ', 'https://i.ibb.co/7z8crTM/v4.png', 2),
(5, 'Bộ thời trang Túi Khối Màu Thể thao', 254000, 'Phong cách:	Thể thao\r\nMàu sắc:	Nhiều màu\r\nKiểu mẫu:	Khối Màu\r\nLoại Áo:	Áo lót hai dây\r\nCác Loại Bottom:	Quần short\r\nChi tiết:	Dây kéo, Túi\r\nLoại Phù hợp:	Phù hợp thường\r\nViền :	Scoop Cổ\r\nChiều dài tay:	Không tay\r\nMùa:	Mùa Hè\r\nMỏng:	Không\r\nVật liệu:	Bông\r\nThành phần:	100% Bông\r\nSợi vải:	Không căng', 'https://i.ibb.co/4SjDLTF/image.png', 1),
(6, 'Áo liền quần Nút răng cưa Thanh lịch', 400000, 'Phong cách:	Thanh lịch\r\nMàu sắc:	Đen và trắng\r\nKiểu mẫu:	răng cưa\r\nChiều dài:	Dài\r\nKiểu:	Xe tăng\r\nChi tiết:	Chia, Nút\r\nLoại Phù hợp:	Phù hợp thường\r\nViền :	Đứng cổ áo\r\nChiều dài tay:	Không tay\r\nVòng eo:	Vòng eo cao\r\nMùa:	Mùa Xuân/ Mùa Thu\r\nMỏng:	Không\r\nTrang phục Aribian :	Vâng\r\nVật liệu:	Polyester\r\nThành phần:	95% Polyester, 5% Spandex\r\nSợi vải:	Căng nhẹ', 'https://i.ibb.co/FVWzmBn/image.png', 1),
(7, ' Quần Dây kéo màu trơn Thanh lịch', 320000, 'Phong cách:	Thanh lịch\r\nMàu sắc:	màu đen\r\nKiểu mẫu:	màu trơn\r\nChiều dài:	Dài\r\nKiểu:	quần ống rộng\r\nChi tiết:	Dây kéo\r\nMùa:	Mùa Xuân/Mùa Hè/ Mùa Thu\r\nLoại Phù hợp:	Rộng\r\nVòng eo:	Vòng eo cao\r\nMỏng:	Không\r\nloại đóng kín:	Dây kéo tích hợp\r\nVật liệu:	Polyester\r\nThành phần:	100% Polyester\r\nSợi vải:	Không căng', 'https://i.ibb.co/KG8Jk60/image.png', 1),
(8, 'Quần short Denim Nút màu trơn màu đen', 290000, 'Màu sắc:	màu đen\r\nKiểu mẫu:	màu trơn\r\nKiểu:	Chân thẳng\r\nChi tiết:	Nút, Túi\r\nLoại Phù hợp:	Phù hợp thường\r\nVòng eo:	Tự nhiên\r\nMỏng:	Không\r\nloại đóng kín:	Nút bay\r\nVật liệu:	Denim/jean\r\nThành phần:	85% Bông, 15% Polyester\r\nSợi vải:	Không căng', 'https://i.ibb.co/VjBgM8K/image.png', 1),
(9, 'Quần short Denim Túi màu trơn', 343000, 'Màu sắc:	Rửa nhẹ\r\nKiểu mẫu:	màu trơn\r\nKiểu:	Quần giữa\r\nChi tiết:	Bị tách, Hem thô, Túi, Denim mùa giặt\r\nLoại Phù hợp:	Phù hợp thường\r\nVòng eo:	Vòng eo cao\r\nMỏng:	Không\r\nloại đóng kín:	Dây kéo tích hợp\r\nVật liệu:	Denim/jean\r\nThành phần:	85% Bông, 15% Polyester\r\nSợi vải:	Không căng', 'https://i.ibb.co/JzdS1nt/image.png', 1),
(10, 'Áo nỉ Nam Dây kéo màu trơn Giải trí', 349000, 'Phong cách:	Giải trí\r\nMàu sắc:	màu đen\r\nKiểu mẫu:	màu trơn\r\nChiều dài:	Thường xuyên\r\nKiểu:	Áo chui\r\nChi tiết:	Dây kéo, Cao thấp\r\nLoại Phù hợp:	Phù hợp thường\r\nViền :	áo có mũ\r\nChiều dài tay:	Tay áo dài\r\nLoại tay áo:	Tay thường\r\nMùa:	Mùa Xuân/ Mùa Thu\r\nMỏng:	Không\r\nVật liệu:	Bông\r\nThành phần:	50% Bông, 50% Polyester\r\nSợi vải:	Căng nhẹ', 'https://i.ibb.co/XV2FSjT/image.png', 2),
(11, 'Áo nỉ Nam Sọc Giải trí', 339000, 'Phong cách:	Sẵn sàng\r\nMàu sắc:	màu đen\r\nKiểu mẫu:	Sọc, Lá thư\r\nChiều dài:	Thường xuyên\r\nKiểu:	Áo chui\r\nChi tiết:	Ruy băng, Sọc bên\r\nLoại Phù hợp:	Phù hợp thường\r\nViền :	Vòng cổ\r\nChiều dài tay:	Tay áo dài\r\nLoại tay áo:	Tay thường\r\nMùa:	Mùa Xuân/ Mùa Thu\r\nMỏng:	Không\r\nVật liệu:	Polyester\r\nThành phần:	95% Polyester, 5% Spandex\r\nSợi vải:	Căng nhẹ', 'https://i.ibb.co/BPmncj8/image.png', 2),
(12, 'Đồ bơi Nam Dây kéo Tất cả trên in Kỳ nghỉ', 178000, 'Phong cách:	Kỳ nghỉ\r\nMàu sắc:	Nhiều màu\r\nKiểu mẫu:	Tất cả trên in\r\nKiểu:	Đáy\r\nChi tiết:	Dây kéo, Túi\r\nCác Loại Bottom:	Quần short\r\nVải lót:	Lót\r\nVật liệu:	Polyester\r\nThành phần:	100% Polyester\r\nSợi vải:	Không căng', 'https://i.ibb.co/SxnGdW1/image.png', 2),
(13, '\r\nSHEIN UNISEX Áo thun trơn trễ vai nam\r\n', 207000, 'Phong cách:	Giải trí, Cơ bản\r\nMàu sắc:	trắng\r\nKiểu mẫu:	màu trơn\r\nChiều dài:	Thường xuyên\r\nLoại Phù hợp:	Phù hợp thường\r\nViền :	Vòng cổ\r\nMùa:	Mùa Hè\r\nChiều dài tay:	Ngắn tay\r\nLoại tay áo:	Thả vai\r\nMỏng:	Không\r\nLoại túi váy:	Áo chui\r\nVật liệu:	Bông\r\nThành phần:	100% Bông\r\nSợi vải:	Không căng', 'https://i.ibb.co/QHJt7Vs/image.png', 2),
(14, 'SHEIN Tops Nam Dây kéo màu trơn Giải trí', 170000, 'Phong cách:	Giải trí\r\nMàu sắc:	Xám\r\nKiểu mẫu:	màu trơn\r\nChiều dài:	Thường xuyên\r\nChi tiết:	Dây kéo\r\nLoại Phù hợp:	Phù hợp thường\r\nViền :	áo có mũ\r\nMùa:	Mùa Hè\r\nChiều dài tay:	Không tay\r\nMỏng:	Không\r\nLoại túi váy:	Áo chui\r\nVật liệu:	Polyester\r\nThành phần:	90% Polyester, 10% Spandex\r\nSợi vải:	Căng nhẹ\r\nHướng dẫn Bảo quản:	Máy Giặt Lạnh', 'https://i.ibb.co/NxGyTVY/image.png', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `hoten` text NOT NULL,
  `email` varchar(200) NOT NULL,
  `tk` varchar(100) NOT NULL,
  `mk` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `hoten`, `email`, `tk`, `mk`) VALUES
(1, 'abc', 'abc@gmail.com', 'abc123', '$2y$10$kX.3cX86sbJZw7auDQA8EeY34pZYhajGwwCohBkZiFnlvXaWoo4Gq'),
(2, 'Van Khoa', 'vk@gmail.com', 'vk123', '$2y$10$H8e/3CdUDRF/nog.OvLwj.r0dQxDQmQuc5GsxqKIMF0DK97imJB4C'),
(6, 'Tien Bip', 'tienbip@gmail.com', 'tienbip123', '$2y$10$XZfgsugs4RHBIRBk9IeMwuJIRYc09lg1U55W4Pcglxnq3enjgDLra'),
(8, 'Long Troc', 'longt@gmail.com', 'ltrong123', '$2y$10$mQ6BEbb9HqMhl/pG1yN7We6lRtBtlEA5RctmQrvjkCf1tTbZYAfTG'),
(9, 'tdtd', 'kdnsak@gmail.com', 'tdtd123', '$2y$10$etNRqDTBWUphoRoaQ2/CBePYEa6C4vz66OWn5tRf7RtMhZlkhtGny'),
(11, 'tiendattroc\n', 'tdathp@gmail.com', 'tdathp123', '$2y$10$8IEz3MujfB9OYqXpG/QoVO78UYG0q3neXFSz8a8aFahsmy/1xLJ7u'),
(12, 'tao', 'tao123@gmail.com', 'tao123', '$2y$10$l56Uhg0bbHs/D1oWsMV3JeyhEKUHjfuyojQzmlNXzvCtjMFdrdQqq'),
(13, 'mdeo', 'mdeo@gmail.com', 'mden', '$2y$10$LZ7mGnSlOnR4k4kuqXgdaeTTngnXCrPVGp9jKxcvXpdA59KyaP7cC');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `yeuthich`
--

CREATE TABLE `yeuthich` (
  `id` int(11) NOT NULL,
  `tenkhach` text NOT NULL,
  `idsp` int(11) NOT NULL,
  `taoluc` text NOT NULL,
  `noti` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `loaimonan`
--
ALTER TABLE `loaimonan`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `yeuthich`
--
ALTER TABLE `yeuthich`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT cho bảng `yeuthich`
--
ALTER TABLE `yeuthich`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
