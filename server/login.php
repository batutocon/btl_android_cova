<?php
require "DataBase.php";
$db = new DataBase();
if (isset($_POST['tk']) && isset($_POST['mk'])) {
    if ($db->dbConnect()) {
        if ($db->logIn("users", $_POST['tk'], $_POST['mk'])) {
            echo "Success";
        } else echo "Username or Password wrong";
    } else echo "Error: Database connection";
} else echo "All fields are required";
?>
