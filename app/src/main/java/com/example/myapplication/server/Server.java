package com.example.myapplication.server;

public class Server {
    public static String localhost="192.168.1.50";
    public static String linklogin="http://"+localhost+":8080/server/login.php";
    public static String linksignup="http://"+localhost+":8080/server/signup.php";
    public static String linkloaisp="http://"+localhost+":8080/server/getloaisp.php";
    public static String linkallyeuthich="http://"+localhost+":8080/server/getAllyeuthich.php";
    public static String linkmonanmoi="http://"+localhost+":8080/server/getspmoi.php";
    public static String linksbycate="http://"+localhost+":8080/server/getspbycate.php";
    public static String linksthemyeuthich="http://"+localhost+":8080/server/themyeuthich.php";
    public static String linkxoayeuthich="http://"+localhost+":8080/server/xoayeuthich.php";
    public static String linkchitietsp="http://"+localhost+":8080/server/getchitietsp.php";
}

