package com.example.myapplication.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.myapplication.ChitietMonanActivity;
import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.Sanpham;
import com.example.myapplication.model.YeuthichAdapter;
import com.example.myapplication.model.Yeuthich;
import com.example.myapplication.server.Server;
import com.vishnusivadas.advanced_httpurlconnection.PutData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentYeuthich extends Fragment {
    ListView lvyeuthich;
    TextView txtthongbao;
    YeuthichAdapter yeuthichAdapter;
    public FragmentYeuthich() {

    }

    @Override
    public void onResume() {
        super.onResume();

        ArrayList<Yeuthich> rs= new ArrayList<>(MainActivity.mangh.size());
        for(int i = MainActivity.mangh.size() - 1; i>= 0; i--){
            rs.add(MainActivity.mangh.get(i));
        }
        yeuthichAdapter=new YeuthichAdapter(rs,getActivity().getApplicationContext());
        lvyeuthich.setAdapter(yeuthichAdapter);
        checkData();

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_yeuthich, container, false);
        anhxa(view);
        checkData();
        return view;
    }

    private void checkData() {
        if(MainActivity.mangh.size()<=0){
            yeuthichAdapter.notifyDataSetChanged();
            txtthongbao.setVisibility(View.VISIBLE);
            lvyeuthich.setVisibility(View.INVISIBLE);
        }
        else {
            yeuthichAdapter.notifyDataSetChanged();
            txtthongbao.setVisibility(View.INVISIBLE);
            lvyeuthich.setVisibility(View.VISIBLE);
        }
    }

    public void anhxa(View view) {
        lvyeuthich=(ListView) view.findViewById(R.id.lvyeuthich);
        txtthongbao=(TextView) view.findViewById(R.id.tvthongbao);
        ArrayList<Yeuthich> rs= new ArrayList<>(MainActivity.mangh.size());
        for(int i = MainActivity.mangh.size() - 1; i>= 0; i--){
            rs.add(MainActivity.mangh.get(i));
        }
        yeuthichAdapter=new YeuthichAdapter(rs,getActivity().getApplicationContext());
        lvyeuthich.setAdapter(yeuthichAdapter);

        lvyeuthich.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
//                // selected item
                Yeuthich selectedItem = (Yeuthich) parent.getItemAtPosition(position);
                //Start ProgressBar first (Set visibility VISIBLE)
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        String[] field = new String[1];
                        field[0] = "id";
                        String[] data = new String[1];
                        data[0] = String.valueOf(selectedItem.getIdsp());

                        PutData putData = new PutData(Server.linkchitietsp, "GET", field, data);
                        if (putData.startPut()) {
                            if (putData.onComplete()) {
                                String resp = putData.getResult();
                     //   Toast.makeText(getActivity(),resp.toString(), Toast.LENGTH_SHORT).show();

                                int id=0;
                                String tensp="",loai="",hasp="",motasp ="";
                                int idsp=0;
                                try {
                                    JSONArray jsonArray= new JSONArray(resp);
                                    JSONObject jsonObject=jsonArray.getJSONObject(0);
                                    id= jsonObject.getInt("id");
                                    tensp=jsonObject.getString("tensp");
                                    loai=jsonObject.getString("loai");
                                    motasp=jsonObject.getString("motasp");
                                    hasp=jsonObject.getString("hinhanh");
                                    idsp=jsonObject.getInt("idsp");
                                    Sanpham sp = new Sanpham(id,tensp,loai,motasp,hasp,idsp);
                                    Intent intent=new Intent(getActivity().getApplicationContext(), ChitietMonanActivity.class).putExtra("thongtinsp",sp);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    getActivity().startActivity(intent);
                                } catch (Exception e) {
                                    Toast.makeText(getActivity(),e.toString(), Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                });
            }
        });
    }
}
