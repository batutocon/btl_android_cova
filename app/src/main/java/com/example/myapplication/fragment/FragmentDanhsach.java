package com.example.myapplication.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.myapplication.ChitietMonanActivity;
import com.example.myapplication.R;
import com.example.myapplication.SanphamByCateActivity;
import com.example.myapplication.model.Category;
import com.example.myapplication.model.CategoryAdapter;
import com.example.myapplication.model.Category;
import com.example.myapplication.server.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentDanhsach extends Fragment implements CategoryAdapter.Selected{

    ArrayList<Category> cateArrayList;
    CategoryAdapter cateAdapter;
    RecyclerView recyclerView;
    int id=0;

    public FragmentDanhsach() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_danhsach, container, false);
        anhxa(view);
        GetDataCate();
        return view;
    }

    public void GetDataCate(){
        RequestQueue requestQueue= Volley.newRequestQueue(getActivity().getApplicationContext());
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Request.Method.GET, Server.linkloaisp,null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if(response!=null){
                    int ID=0;
                    String Ten="";
                    String Hinhanh="",Motasp="";
                    for(int i=0;i<response.length();i++){
                        try{
                            JSONObject jsonObject=response.getJSONObject(i);
                            ID= jsonObject.getInt("id");
                            Ten=jsonObject.getString("tenloai");
                            Hinhanh=jsonObject.getString("hinhanh");
                            cateArrayList.add(new Category(ID,Ten,Hinhanh));
                            cateAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(getActivity(),error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(jsonArrayRequest);
    }

    private void anhxa(View v) {
        recyclerView=(RecyclerView) v.findViewById(R.id.recvcate);
        //SP new
        cateArrayList=new ArrayList<>();
        cateAdapter=new CategoryAdapter(getActivity().getApplicationContext(),cateArrayList,this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity().getApplicationContext(),2));
        recyclerView.setAdapter(cateAdapter);
    }

    @Override
    public void Selected(Category ca) {
        Intent intent=new Intent(getActivity().getApplicationContext(), SanphamByCateActivity.class).putExtra("thongtinca",ca);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getActivity().startActivity(intent);
    }
}

