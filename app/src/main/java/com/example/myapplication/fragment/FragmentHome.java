package com.example.myapplication.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.myapplication.ChitietMonanActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.Sanpham;
import com.example.myapplication.model.SanphamMoiAdapter;
import com.example.myapplication.server.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHome extends Fragment implements SanphamMoiAdapter.Selected{

    ViewFlipper viewFlipper;
    ArrayList<Sanpham> sanphamArrayList;
    SanphamMoiAdapter sanphamMoiAdapter;
    RecyclerView recyclerView;
    int id=0;
    String tenloaisp="",haloaisp="";

    public FragmentHome() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        viewFlipper=(ViewFlipper) view.findViewById(R.id.viewflipper);
        anhxa(view);

        // Chay mang quang cao
        ArrayList<Integer> mangquangcao=new ArrayList<>();
        mangquangcao.add(R.drawable.slider_image1);
        mangquangcao.add(R.drawable.slider_image2);
        mangquangcao.add(R.drawable.slider_image3);
        for(int i=0;i<mangquangcao.size();i++){
            actionFlipper(mangquangcao.get(i));
        }

        GetDataSp();
        return view;
    }
    private void anhxa(View v) {
        viewFlipper=(ViewFlipper) v.findViewById(R.id.viewflipper);
        recyclerView=(RecyclerView) v.findViewById(R.id.recv);
        //SP new
        sanphamArrayList=new ArrayList<>();
        sanphamMoiAdapter=new SanphamMoiAdapter(getActivity().getApplicationContext(),sanphamArrayList,this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity().getApplicationContext(),2));
        recyclerView.setAdapter(sanphamMoiAdapter);

//        if(mangh!=null){
//        }
//        else{
//            mangh=new ArrayList<>();
//        }
    }
    private  void actionFlipper(int imge){
        ImageView imageView=new ImageView(getActivity().getApplicationContext());
        imageView.setBackgroundResource(imge);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        viewFlipper.addView(imageView);
        viewFlipper.setFlipInterval(5000);
        viewFlipper.setAutoStart(true);
    }
    public void GetDataSp(){
        RequestQueue requestQueue= Volley.newRequestQueue(getActivity().getApplicationContext());
        JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(Request.Method.GET, Server.linkmonanmoi,null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if(response!=null){
                    int ID=0;
                    String Tensp="";
                    String Loai="";
                    String Hinhanh="",Motasp="";
                    int IDsp=0;
                    for(int i=0;i<response.length();i++){
                        try{
                            JSONObject jsonObject=response.getJSONObject(i);
                            ID= jsonObject.getInt("id");
                            Tensp=jsonObject.getString("tensp");
                            Loai=jsonObject.getString("loai");
                            Motasp=jsonObject.getString("motasp");
                            Hinhanh=jsonObject.getString("hinhanh");
                            IDsp=jsonObject.getInt("idsp");
                            sanphamArrayList.add(new Sanpham(ID,Tensp,Loai,Motasp,Hinhanh,IDsp));
                            sanphamMoiAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(getActivity(),error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(jsonArrayRequest);
    }

    @Override
    public void Selected(Sanpham sp) {
        Intent intent=new Intent(getActivity().getApplicationContext(), ChitietMonanActivity.class).putExtra("thongtinsp",sp);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getActivity().startActivity(intent);
    }
}
