package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.myapplication.fragment.FargmentNavigationAdapter;
import com.example.myapplication.model.Sanpham;
import com.example.myapplication.model.YeuthichAdapter;
import com.example.myapplication.model.Yeuthich;
import com.example.myapplication.server.Server;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private BottomNavigationView navBotView;
    private ViewPager viewPager;
    private FargmentNavigationAdapter adapter;
    public static ArrayList<Yeuthich> mangh;
    public static String username="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager=findViewById(R.id.viewPager);
        navBotView=findViewById(R.id.navigation);
        if(mangh!=null){

        }
        else{
            mangh =new ArrayList<>();
                GetDataYeuthich();

        }
        username= getIntent().getStringExtra("username");

        adapter=new FargmentNavigationAdapter(getSupportFragmentManager(),
                FargmentNavigationAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(adapter);
        navBotView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.mYeuthich:viewPager.setCurrentItem(0);
                     break;
                    case R.id.mGanday:viewPager.setCurrentItem(1);
                        break;
                    case R.id.mDanhba:viewPager.setCurrentItem(2);
                        break;
                    case R.id.mBanphim:viewPager.setCurrentItem(3);
                        break;
                }
                return true;
            }
        });

    }

    public void GetDataYeuthich(){
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        String url= Server.linkallyeuthich;
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                int id=0,status=0,idsp=0;
                String tensp="",loai="",hasp="",time ="";
                int IDsp=0;
                    try {
                        JSONArray jsonArray= new JSONArray(response);
                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject=jsonArray.getJSONObject(i);
                            id= jsonObject.getInt("id");
                            idsp=jsonObject.getInt("idsp");
                            tensp=jsonObject.getString("tensp");
                            loai=jsonObject.getString("loai");
                            hasp=jsonObject.getString("hasp");
                            time=jsonObject.getString("time");
                            status=jsonObject.getInt("status");
                            mangh.add(new Yeuthich(id,idsp,tensp,loai,hasp,time,status));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> param=new HashMap<String,String>();
                param.put("tenkhach",String.valueOf(username.trim()));
                return param;
            }
        };
        requestQueue.add(stringRequest);
    }
}
