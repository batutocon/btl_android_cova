package com.example.myapplication.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;


public class YeuthichAdapter extends BaseAdapter {
    ArrayList<Yeuthich> arrYeuthich;
    Context context;

    public YeuthichAdapter(ArrayList<Yeuthich> arrYeuthich, Context context) {
        this.arrYeuthich = arrYeuthich;
        this.context = context;
    }

    @Override
    public int getCount() {
        return arrYeuthich.size();
    }

    @Override
    public Object getItem(int i) {
        return arrYeuthich.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public  class ViewHoder{
        public TextView txttenYeuthich,txtgiaYeuthich,tvtaolucyeuthich;
        public ImageView imgspYeuthich;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHoder viewHoder=null;
        if(view==null){
            viewHoder =new ViewHoder();
            LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(R.layout.item_yeuthich,null);
            viewHoder.txttenYeuthich=(TextView) view.findViewById(R.id.tvtenyeuthich);
            viewHoder.txtgiaYeuthich=(TextView) view.findViewById(R.id.tvloaiyeuthich);
            viewHoder.imgspYeuthich=(ImageView) view.findViewById(R.id.imgspyeuthich);
            viewHoder.tvtaolucyeuthich=(TextView) view.findViewById(R.id.tvtaolucyeuthich);

            view.setTag(viewHoder);
        }
        else {
            viewHoder=(ViewHoder) view.getTag();
        }

        Yeuthich yt= (Yeuthich) getItem(i);
        if(yt.getStatus()==1){
            view.setBackgroundColor(context.getResources().getColor(android.R.color.holo_blue_light));;
        }
        viewHoder.txttenYeuthich.setText(yt.getTensp());
        viewHoder.tvtaolucyeuthich.setText("Lưu lúc: "+yt.getTime());
        viewHoder.txtgiaYeuthich.setText(yt.getLoai());
        Picasso.get().load(yt.getHasp())
                .placeholder((R.drawable.ic_launcher_foreground))
                .error(R.drawable.error)
                .into(viewHoder.imgspYeuthich);
        return view;
    }
}