package com.example.myapplication.model;

import java.io.Serializable;

public class Sanpham implements Serializable {
    public int ID;
    public String Tensp;
    public String Loai;
    public String Motasp;
    public String Hinhanh;
    public int IDsp;

    public Sanpham(int ID, String tensp, String loai, String motasp, String hinhanh, int IDsp) {
        this.ID = ID;
        Tensp = tensp;
        Loai = loai;
        Motasp = motasp;
        Hinhanh = hinhanh;
        this.IDsp = IDsp;

    }
    public Sanpham() {

    }
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getTensp() {
        return Tensp;
    }

    public void setTensp(String tensp) {
        Tensp = tensp;
    }

    public String getLoai() {
        return Loai;
    }

    public void setLoai(String loai) {
        Loai = loai;
    }

    public String getMotasp() {
        return Motasp;
    }

    public void setMotasp(String motasp) {
        Motasp = motasp;
    }

    public String getHinhanh() {
        return Hinhanh;
    }

    public void setHinhanh(String hinhanh) {
        Hinhanh = hinhanh;
    }

    public int getIDsp() {
        return IDsp;
    }

    public void setIDsp(int IDsp) {
        this.IDsp = IDsp;
    }
}
