package com.example.myapplication.model;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.ChitietMonanActivity;
import com.example.myapplication.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class SanphamByCateAdapter extends RecyclerView.Adapter<SanphamByCateAdapter.ItemHolder> implements Filterable {
        Context context;
        ArrayList<Sanpham> arrsp;
        ArrayList<Sanpham> arrsp_search;

        public SanphamByCateAdapter( Context context, ArrayList<Sanpham> arrsp) {
            this.context = context;
            this.arrsp = arrsp;
            this.arrsp_search=arrsp;
        }

        @Override
        public ItemHolder onCreateViewHolder( ViewGroup parent, int viewType) {
            View v =LayoutInflater.from(parent.getContext()).inflate(R.layout.item_spbycate,null);
            ItemHolder itemHolder=new ItemHolder(v);
            return itemHolder;
        }

        @Override
        public void onBindViewHolder( ItemHolder holder, int position) {
            Sanpham sp=arrsp.get(position);
            holder.txttensp.setText(sp.getTensp());
            holder.txtloaisp.setText("Loại: "+sp.getLoai());
            Picasso.get().load(sp.getHinhanh())
                    .placeholder(R.drawable.ic_launcher_background)
                    .error(R.drawable.error)
                    .into(holder.imgsp);
        }

        @Override
        public int getItemCount() {
            return arrsp.size();
        }

        public class ItemHolder extends RecyclerView.ViewHolder{
            public ImageView imgsp;
            public TextView txttensp,txtloaisp;

            public ItemHolder(View itemView) {
                super(itemView);
                imgsp=(ImageView) itemView.findViewById(R.id.imgbycate);
                txttensp=(TextView) itemView.findViewById(R.id.tvtenspbycate);
                txtloaisp=(TextView) itemView.findViewById(R.id.tvloaispbycate);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(context, ChitietMonanActivity.class);
                        intent.putExtra("thongtinsp",arrsp.get(getAdapterPosition()));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }
                });
            }
        }

        // cho tim kiem
        @Override
        public Filter getFilter() {

            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    String strSearch= constraint.toString();
                    if(strSearch.isEmpty()){
                        arrsp=arrsp_search;
                    }
                    else  {
                        ArrayList<Sanpham> list= new ArrayList<>();
                        for(Sanpham sp: arrsp_search){
                            if(sp.getTensp().toLowerCase().contains(strSearch.toLowerCase())){
                                list.add(sp);
                            }
                        }
                        arrsp=list;
                    }

                    FilterResults filterResults= new FilterResults();
                    filterResults.values=arrsp;
                    return filterResults;
                }


                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    arrsp= (ArrayList<Sanpham>) results.values;
                    notifyDataSetChanged();
                }
            };
        }
    }