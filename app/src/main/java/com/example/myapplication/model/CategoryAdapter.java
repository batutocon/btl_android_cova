package com.example.myapplication.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ItemHolder> {

    Context context;
    ArrayList<Category> arrcate;
    Selected selecd;
    public CategoryAdapter(Context context, ArrayList<Category> arrcate, Selected sl) {
        this.context = context;
        this.arrcate = arrcate;
        this.selecd = sl;
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category,null);
        ItemHolder itemHolder=new ItemHolder(v);
        return itemHolder;
    }

    @Override
    public void onBindViewHolder( ItemHolder holder, int position) {
         Category sp=arrcate.get(position);
        holder.tencate.setText("Loại: "+sp.getTenloaisp());
        Picasso.get().load(sp.getHaloaisp())
                     .placeholder(R.drawable.ic_launcher_background)
                     .error(R.drawable.error)
                     .into(holder.imgcate);
    }

    public interface Selected{
        void Selected(Category cate);
    }

    @Override
    public int getItemCount() {
        return arrcate.size();
    }

    public class ItemHolder extends RecyclerView.ViewHolder{
        public ImageView imgcate;
        public TextView tencate;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);
            imgcate=(ImageView) itemView.findViewById(R.id.imgcate);
            tencate=(TextView) itemView.findViewById(R.id.tencate);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selecd.Selected(arrcate.get(getAdapterPosition()));
//                    Intent intent=new Intent(context, ChitietMonanActivity.class);
//                    intent.putExtra("thongtinsp",arrcate.get(getAdapterPosition()));
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    context.startActivity(intent);
                }
            });
        }
    }
}