package com.example.myapplication.model;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.ChitietMonanActivity;
import com.example.myapplication.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class SanphamMoiAdapter extends RecyclerView.Adapter<SanphamMoiAdapter.ItemHolder> {

    Context context;
    ArrayList<Sanpham> arrsp;
    Selected selecd;
    public SanphamMoiAdapter(Context context, ArrayList<Sanpham> arrsp,Selected sl) {
        this.context = context;
        this.arrsp = arrsp;
        this.selecd = sl;
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_monmoi,null);
        ItemHolder itemHolder=new ItemHolder(v);
        return itemHolder;
    }

    @Override
    public void onBindViewHolder( ItemHolder holder, int position) {
         Sanpham sp=arrsp.get(position);
        holder.txttensp.setText(sp.getTensp());
        holder.txttensp.setText(sp.getTensp());
        holder.txtloaisp.setText("Loại: "+sp.getLoai());
        Picasso.get().load(sp.getHinhanh())
                     .placeholder(R.drawable.ic_launcher_background)
                     .error(R.drawable.error)
                     .into(holder.imgsp);
    }

    public interface Selected{
        void Selected(Sanpham sp);
    }

    @Override
    public int getItemCount() {
        return arrsp.size();
    }

    public class ItemHolder extends RecyclerView.ViewHolder{
        public ImageView imgsp;
        public TextView txttensp,txtloaisp;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);
            imgsp=(ImageView) itemView.findViewById(R.id.imgspmoi);
            txttensp=(TextView) itemView.findViewById(R.id.tvtenspmoi);
            txtloaisp=(TextView) itemView.findViewById(R.id.tvloaispmoi);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selecd.Selected(arrsp.get(getAdapterPosition()));
//                    Intent intent=new Intent(context, ChitietMonanActivity.class);
//                    intent.putExtra("thongtinsp",arrsp.get(getAdapterPosition()));
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    context.startActivity(intent);
                }
            });
        }
    }
}