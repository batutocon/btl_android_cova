package com.example.myapplication.model;

public class Yeuthich {
    public int id;
    public int idsp;
    public String tensp;
    public String loai;
    public String hasp;
    public String time;
    int status;

    public Yeuthich(int idsp, String tensp, String hasp, String time,int status) {
        this.idsp = idsp;
        this.tensp = tensp;
        this.hasp = hasp;
        this.time = time;
        this.status = status;
    }
    public Yeuthich(int id, int idsp, String tensp,String loai, String hasp, String time,int status) {
        this.id = id;
        this.idsp = idsp;
        this.tensp = tensp;
        this.loai = loai;
        this.hasp = hasp;
        this.time = time;
        this.status = status;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdsp() {
        return idsp;
    }

    public void setIdsp(int idsp) {
        this.idsp = idsp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTensp() {
        return tensp;
    }

    public void setTensp(String tensp) {
        this.tensp = tensp;
    }
    public String getLoai() {
        return loai;
    }

    public void setLoai(String loai) {
        this.loai = loai;
    }


    public String getHasp() {
        return hasp;
    }

    public void setHasp(String hasp) {
        this.hasp = hasp;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
