package com.example.myapplication;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.myapplication.model.Sanpham;
import com.example.myapplication.model.Yeuthich;
import com.example.myapplication.model.YeuthichAdapter;
import com.example.myapplication.server.CheckConnection;
import com.example.myapplication.server.Server;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class ChitietMonanActivity extends AppCompatActivity {
    Toolbar toolbarctsp;
    ImageView imgctsp;
    TextView tvten,tvloai,tvmt;
    Button btnmua;
    int id=0;
    String ten="";
    String loai="";
    String ha="",mota="";
    int idsp=0;
    int mYear ,mMonth,mDay;
    String time = "";
    boolean mFollowing= false;
    int sttarr;
    String timedb;
    boolean checktime = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chitiet_sanpham);
        anhxa();

        btnmua.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (!mFollowing)
                {
                    enventLuu();
                    mFollowing = true;
                    btnmua.setText("Đã lưu");
                }
                else
                {
                    xoaDB();
                    mFollowing = false;
                    btnmua.setText("Lưu");
                }
            }
        });
        start();
    }

    public void onResume() {
        super.onResume();
        for(int i=0;i<MainActivity.mangh.size();i++){
            if(MainActivity.mangh.get(i).getIdsp()==id){
                mFollowing=true;
            }
        }
        if (!mFollowing)
        {
            btnmua.setText("Lưu");
        }
        else
        {
            btnmua.setText("Đã Lưu");
        }
    }
    public void enventLuu(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(ChitietMonanActivity.this);
        dialog.setTitle("Bạn có muốn thêm nhắc nhở");
        final EditText textTime = new EditText(ChitietMonanActivity.this);
        //nhap ngay
        textTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c=Calendar.getInstance();
                mYear=c.get(Calendar.YEAR);
                mMonth=c.get(Calendar.MONTH);
                mDay=c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog=new DatePickerDialog(ChitietMonanActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                textTime.setText(dayOfMonth+"/"+(month+1)+"/"+year);
                            }
                        },mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        dialog.setView(textTime);
        dialog.setPositiveButton("Thêm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                time = textTime.getText().toString();
                themthongbao();
                luuSP();
            }
        });
        dialog.setNegativeButton("Không thêm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                time = textTime.getText().toString();
                checktime = true;
                luuSP();
            }
        });
        dialog.show();

    }

    private void start() {
        Sanpham sp= (Sanpham) getIntent().getSerializableExtra("thongtinsp");
        id=sp.getID();
        ten=sp.getTensp();
        loai=sp.getLoai();
        ha=sp.getHinhanh();
        mota=sp.getMotasp();
        idsp=sp.getIDsp();
        tvten.setText(ten);
        tvloai.setText("Loại món ăn: "+loai);
        tvmt.setText(mota);
        Picasso.get().load(ha)
                .placeholder(R.drawable.ic_launcher_foreground)
                .error(R.drawable.error)
                .into(imgctsp);
    }

    public void xoaDB(){
        MainActivity.mangh.removeIf(t -> t.idsp == id);
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        String url= Server.linkxoayeuthich;
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                time="";
                Toast.makeText(ChitietMonanActivity.this,response,Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> param=new HashMap<String,String>();
                param.put("username",String.valueOf(MainActivity.username.trim()));
                param.put("sanphamid",String.valueOf(id));
                return param;
            }
        };
        requestQueue.add(stringRequest);
    }

    public void themthongbao(){
        String title="Hôm nay chúng ta cùng nấu ăn nhé!";
        String timenoti=time;
        String desc="Nấu món: "+ten;

        String [] time_spilt=timenoti.split("/");
        int date=Integer.parseInt(time_spilt[0]);
        int month=Integer.parseInt(time_spilt[1])-1;
        int year=Integer.parseInt(time_spilt[2]);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(year,month,date);

        AlarmManager am = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(ChitietMonanActivity.this,
                MyReceiver.class);
        intent.putExtra("myAction", "mDoNotify");
        intent.putExtra("Title",title);
        intent.putExtra("Description", desc);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(ChitietMonanActivity.this,
                0, intent, 0);
        am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        //finish();
    }

    private void luuSP(){
        RequestQueue requestQueue= Volley.newRequestQueue(ChitietMonanActivity.this);
        Date datenow = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String strDateNow = formatter.format(datenow);
        timedb= time;

        if(time.trim()=="") {
            timedb="null";
        }
        int stt=0;
        if(!checktime){
            stt=1;
        }
        sttarr = stt;
        final String timearr = strDateNow;
        String url = Server.linksthemyeuthich+"?tenkhach="+MainActivity.username+"&idsp="+id+"&taoluc="+strDateNow+"&noti="+timedb+"&status="+stt;
        StringRequest request= new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String respone) {
                int idyeuthich=Integer.parseInt(respone.trim());
                MainActivity.mangh.add(new Yeuthich(idyeuthich,id,ten,loai,ha,timearr,sttarr));
                time="";
                Toast.makeText(ChitietMonanActivity.this,"Đã thêm vào mục yêu thích",Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                time="";
//                Toast.makeText(getActivity(),error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);
    }

    private void ActionToolBar() {
        setSupportActionBar(toolbarctsp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarctsp.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:break;
        }

        return super.onOptionsItemSelected(item);
    }
    private void anhxa() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imgctsp=(ImageView) findViewById(R.id.imgctsp);
        tvten=(TextView) findViewById(R.id.tenctsp);
        tvloai=(TextView) findViewById(R.id.loaictsp);
        tvmt=(TextView) findViewById(R.id.mtctsp);
        btnmua=(Button) findViewById(R.id.btnmua);
    }

}