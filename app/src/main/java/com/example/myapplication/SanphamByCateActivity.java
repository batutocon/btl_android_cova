package com.example.myapplication;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.myapplication.model.Category;
import com.example.myapplication.model.Sanpham;
import com.example.myapplication.model.SanphamByCateAdapter;
import com.example.myapplication.model.Yeuthich;
import com.example.myapplication.server.Server;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class SanphamByCateActivity extends AppCompatActivity {

    int id = 0;
    RecyclerView recvall;
    SanphamByCateAdapter searchAdapter;
    ArrayList<Sanpham> arrall;
    SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sanphambycate);
        anhxa();
        start();
        GetDataSpbyCate();
    }

    private void start() {
        Category ca = (Category) getIntent().getSerializableExtra("thongtinca");
        id = ca.getId();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }

    public void GetDataSp() {
        RequestQueue requestQueue = Volley.newRequestQueue(SanphamByCateActivity.this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, Server.linkmonanmoi, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (response != null) {
                    int ID = 0;
                    String Tensp = "";
                    String Loai = "";
                    String Hinhanh = "", Motasp = "";
                    int IDsp = 0;
                    for (int i = 0; i < response.length(); i++) {
                        try {
                            JSONObject jsonObject = response.getJSONObject(i);
                            ID = jsonObject.getInt("id");
                            Tensp = jsonObject.getString("tensp");
                            Loai = jsonObject.getString("loai");
                            Motasp = jsonObject.getString("motasp");
                            Hinhanh = jsonObject.getString("hinhanh");
                            IDsp = jsonObject.getInt("idsp");
                            arrall.add(new Sanpham(ID, Tensp, Loai, Motasp, Hinhanh, IDsp));
                            searchAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(getActivity(),error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(jsonArrayRequest);
    }

    public void GetDataSpbyCate() {
        RequestQueue requestQueuebycate = Volley.newRequestQueue(SanphamByCateActivity.this);
        String url = Server.linksbycate;
        try {
            StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    int spid=0,spIDsp=0;
                    String spTensp="",spLoai="",spMotasp="",spHinhanh ="";
                    try {
                        JSONArray jsonArray= new JSONArray(response);
                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject=jsonArray.getJSONObject(i);
                            spid = jsonObject.getInt("id");
                            spTensp = jsonObject.getString("tensp");
                            spLoai = jsonObject.getString("loai");
                            spMotasp = jsonObject.getString("motasp");
                            spHinhanh = jsonObject.getString("hinhanh");
                            spIDsp = jsonObject.getInt("idsp");
                            arrall.add(new Sanpham(spid, spTensp, spLoai, spMotasp, spHinhanh, spIDsp));
                            searchAdapter.notifyDataSetChanged();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {
                @Nullable
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> param = new HashMap<String, String>();
                    param.put("clientcate", String.valueOf(id));
                    return param;
                }
            };
            requestQueuebycate.add(stringRequest);
        } catch (Exception ex) {
            Toast.makeText(SanphamByCateActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void anhxa() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recvall = (RecyclerView) findViewById(R.id.recvall);
        arrall = new ArrayList<>();
        searchAdapter = new SanphamByCateAdapter(getApplicationContext(), arrall);
        recvall.setHasFixedSize(true);
        recvall.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        recvall.setAdapter(searchAdapter);
    }

}